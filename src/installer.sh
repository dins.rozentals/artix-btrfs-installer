#!/bin/sh
# --- Pacman
yes | pacman -Sy --needed parted btrfs-progs


if [[ $encrypted != "n" ]]; then
    fs_pkgs="cryptsetup cryptsetup-dinit btrfs-progs"
else
    fs_pkgs="btrfs-progs grub-btrfs"
fi


# --- Parted
parted -s $my_disk mklabel gpt \
    mkpart fat32 0% 550MiB \
    set 1 esp on
    
parted -s $my_disk \
    mkpart btrfs $((550*1024))MiB 100% 


# --- Formating     
mkfs.fat -n BOOT -F 32 $my_efi
mkfs.btrfs -L ROOT $my_root


# --- Mounting
mount $my_root /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@opt
btrfs subvolume create /mnt/@tmp
btrfs subvolume create /mnt/@var
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@snapshots
if [[ $swap_size -gt 0 ]]; then
    btrfs us cr /mnt/swap
fi
umount -R /mnt


mount -o noatime,compress=lzo,space_cache,subvol=@ $my_root /mnt
mkdir /mnt/{opt,tmp,var,boot,home,swap,.snapshots}
mount -o noatime,compress=lzo,space_cache,subvol=@opt $my_root /mnt/opt
mount -o noatime,compress=lzo,space_cache,subvol=@tmp $my_root /mnt/tmp
mount -o noatime,compress=lzo,space_cache,subvol=@home $my_root /mnt/opt
mount -o noatime,compress=lzo,space_cache,subvol=@snapshots $my_root /mnt/.snapshots
if [[ $swap_size -gt 0 ]]; then
    mount -o nodatacow,subvol=@swap $my_root /mnt/swap
fi
mount -o nodatacow,subvol=@var $my_root /mnt/var


mount $part1 /mnt/boot




#  --- Base system and kernel
[[ $(grep 'vendor' /proc/cpuinfo) == *"Intel"* ]] && ucode="intel-ucode"
[[ $(grep 'vendor' /proc/cpuinfo) == *"Amd"* ]] && ucode="amd-ucode"


basestrap /mnt base base-devel dinit doas elogind-dinit efibootmgr grub dhcpcd wpa_supplicant connman-dinit git $editor $ucode $fs_pkgs
basestrap /mnt linux linux-firmware linux-headers mkinitcpio
fstabgen -U /mnt > /mnt/etc/fstab
