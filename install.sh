#!/bin/sh
# Check boot mode
[[ ! -d /sys/firmware/efi ]] && printf "Not booted in UEFI mode. Aborting..." && exit 1


confirm_password () {
    local pass1="a"
    local pass2="b"
    until [[ $pass1 == $pass2 && $pass2 ]]; do
        printf "$1: " >&2 && read -rs pass1
        printf "\n" >&2
        printf "confirm $1: " >&2 && read -rs pass2
        printf "\n" >&2
    done
}


confirm_name () {
    local name1="a"
    local name2="b"
    until [[ $name1 == $name2 && $name2 ]]; do
        printf "$1: " >&2 && read name1
        printf "confirm $1: " >&2 && read name2
    done
}






# --- Disk selection
while :
do
    sudo lsblk -d | grep -v '^loop'  
    printf "Disk to install to (e.g. sda): " && read x
    my_disk=/dev/"$x"
    echo "Disk: "$my_disk
    [[ -b $my_disk ]] && break
done


my_efi="$my_disk"1
my_root="$my_disk"2
if [[ $my_disk == *"nvme"* ]]; then
    my_efi="$my_disk"p1
    my_root="$my_disk"p2
fi




# --- Swap
until [[ $swap_size =~ ^[0-9]+$ && (($swap_size -gt 0)) && (($swap_size -lt 97)) ]]; do
    printf "Size of swap partition in GiB, 0 disables swap (4): " && read swap_size
    [[ ! $swap_size ]] && swap_size=4
done
#if [[ $swap_size == 0 ]]; then
#    swap_enable=0


# --- Timezone
until [[ -f /usr/share/zoneinfo/$region_city ]]; do
    printf "Region/City (format 'Underwater/Atlantis'): " && read region_city
done




# Host
my_hostname=$(confirm_name "hostname")
#echo "Hostname: "$my_hostname
# Users
root_password=$(confirm_password "root password")
# Editor
echo "What editor to install?(enter for nvim): " && read $editor
[[ ! $editor ]] && editor="neovim"


installvars () {
    echo my_disk=$my_disk my_efi=$my_efi my_root=$my_root swap_size=$swap_size \
        region_city=$region_city my_hostname=$my_hostname root_password=$root_password \
        editor=$editor
}


echo $(installvars)
printf "\nDone with configuration. Installing...\n\n"


# Install
sudo $(installvars) sh src/installer.sh


# Chroot
sudo cp src/iamchroot.sh /mnt/root/ && \
    sudo $(installvars) artix-chroot /mnt /bin/bash -c 'sh /root/iamchroot.sh; rm /root/iamchroot.sh; exit' && \
    printf '\n`sudo artix-chroot /mnt /bin/bash` back into the system to make any final changes.\n\nYou may now poweroff.\n'
