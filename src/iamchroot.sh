#!/bin/sh
# --- Swap
if [[ $swap_size -gt 0 ]]; then
    truncate -s 0 /swap/swapfile
    chattr +C /swap/swapfile
    btrfs property set /swap/swapfile compression none
    dd if=/dev/zero of=/swap/swapfile bs=1G count=$swap_size status=progress
    chmod 600 /swap/swapfile
    mkswap /swap/swapfile
    swapon /swap/swapfile
    printf "\n/swap/swapfile none swap default 0 0" >> /etc/fstab
fi


ln -sf $region_city /etc/localtime
hwclock --systohc


# Localization
printf "en_US.UTF-8 UTF-8\nen_DK.UTF-8 UTF-8\n" >> /etc/locale.gen
locale-gen
printf "LANG=en_DK.UTF-8\n" > /etc/locale.conf
printf "KEYMAP=us\n" > /etc/vconsole.conf


# Host stuff
printf "$my_hostname\n" > /etc/hostname
printf "hostname=\"$my_hostname\"\n" > /etc/conf.d/hostname
printf "\n127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t$my_hostname.localdomain\t$my_hostname\n" > /etc/hosts


# Root user
yes $root_password | passwd


printf "permit persist keepenv :wheel" >> /etc/doas.conf


# Grub
grub-install --target=x86_64-efi --efi-directory=/boot --recheck
grub-mkconfig -o /boot/grub/grub.cfg

cd /opt
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin
makepkg -Si
cd

# Other stuff you should do
ln -s ../connmand /etc/dinit.d/boot.d/
sed -i 's/^MODULES.*$/MODULES=(btrfs)/g' /etc/mkinitcpio.conf
sed -i 's/^HOOKS.*$/HOOKS=(base udev autodetect keyboard keymap modconf block filesystems fsck)/g' /etc/mkinitcpio.conf
mkinitcpio -P
