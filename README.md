# artix-btrfs-installer
Script for install artix with btrfs. Inspired by/forked from https://github.com/Zaechus/artix-installer.

### What is it going to get you
* Base install of Artix with btrfs, doas, yay, connman, dinit

### WIP
* Selection of init system
* Selection of weather or not to encrypt
* Maybe a tui interface
